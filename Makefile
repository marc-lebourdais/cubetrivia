GIT_HASH = $(shell git describe --tags --dirty --always)

.PHONY: all deploy

###
# phony targets

all: deploy

.ONESHELL:
deploy:
	@echo ">> Deploying"
	scp -r build/* pokeytest@pokeytest.com:/var/www/cubetrivia.com/.

