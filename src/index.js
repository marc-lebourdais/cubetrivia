import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'typeface-roboto';
import { BrowserRouter as Router } from 'react-router-dom';
import {
  // createMuiTheme, // this line was replaced
  unstable_createMuiStrictModeTheme as createMuiTheme, // for this one
  ThemeProvider
} from '@material-ui/core/styles';
import AppProvider from './data/context';

const theme = createMuiTheme();

ReactDOM.render(
  <React.StrictMode>
    <AppProvider>
      <ThemeProvider theme={theme}>
        <Router>
          <App />
        </Router>
      </ThemeProvider>
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
